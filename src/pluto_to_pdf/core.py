import os
import argparse
from typing import Any, List, Optional, Tuple, Union

from reportlab.lib import colors
from reportlab.lib.units import cm
from svglib.svglib import svg2rlg
from reportlab.platypus import (
    SimpleDocTemplate,
    Table,
    TableStyle,
    Paragraph,
    ListFlowable,
    Flowable,
    PageTemplate,
    PageBreak,
    NextPageTemplate,
    Frame,
)

from .cover_page import create_cover_page
from .color_text import ColorText
from .flowchart import PlutoFlowchart
from .styles import *


parser = argparse.ArgumentParser(
    description="Creates a flowchart document for a given PLUTO script"
)
parser.add_argument("pluto_filepath", type=str, help="path to the PLUTO script file")
parser.add_argument("output_name", type=str, help="name or path of the output file")

color_text = ColorText()


class PlutoDocument:
    """Class to create a PDF document from a PLUTO script"""

    def __init__(self, pluto_filepath: str, output_name: str):
        self.pluto_filepath = pluto_filepath
        self.output_path = output_name
        self.chart: PlutoFlowchart = None
        self.table_data = []
        self.table_style = []

        if os.path.basename(output_name) == output_name:
            self.output_path = f"./{output_name}"

    def add_row(
        self,
        content: Union[str, Flowable, list],
        step_num: str = "",
        code_pos: Optional[Tuple[int, int]] = None,
        row_styles: List[tuple] = [],
    ):
        """Adds a row to the table of steps"""
        row = {
            "step_num": step_num,
            "content": None,
            "code_pos": f"{code_pos[0]}" if code_pos else "",
        }

        if type(content) == str:
            row["content"] = Paragraph(content.replace("\n", "<br />"), PSTYLE.N)
        else:
            row["content"] = content

        if not row["content"]:
            return

        self.table_data.append([row["step_num"], row["content"], row["code_pos"]])

        if row_styles:
            row = len(self.table_data) - 1
            for style in row_styles:
                self.table_style.append((style[0], (0, row), (-1, row), *style[1:]))

    def build_step_table(self) -> Table:
        """Creates the table of steps"""

        self.table_style += TSTYLE.BASE

        steps_store = self.chart.steps_store

        for node, step in steps_store.steps.items():

            self.add_row(
                Paragraph(step.header, PSTYLE.B),
                step.number_str,
                step.code_pos,
                TSTYLE.STEP,
            )
            # self.table_data.append([step.number_str, Paragraph(step.header, PSTYLE.B)])
            # row = len(self.table_data) - 1
            # self.table_style.append(('LINEABOVE', (0, row), (-1, row), TABLE_BORDER_WIDTH, colors.black))

            if step.details:
                if type(step.details) == list:
                    for detail in step.details:
                        self.add_row(detail)
                else:
                    self.add_row(step.details)

            following_steps = steps_store.get_following_steps(node)
            if following_steps:
                next_steps_p = [Paragraph("Next step(s):", PSTYLE.BI)]
                for step_goto in following_steps:
                    step_nr_str = steps_store.num_lookup[step_goto.to_node]
                    next_steps_p.append(
                        Paragraph(
                            f'-> <font name={FONT_TABLE["b"]}>{step_nr_str}</font> '
                            f"{step_goto.label}",
                            PSTYLE.INDENT,
                        )
                    )
                self.add_row(next_steps_p)

        # Create a table and define its style
        table = Table(self.table_data, **TABLE_ATTRIBS)
        table.setStyle(TableStyle(self.table_style))

        return table

    def clean_up(self):
        """Clean up temporary files"""
        color_text.warning("Cleaning up...")
        os.remove(f"{self.output_path}.svg")
        os.remove(f"{self.output_path}.gv")

    def create(self):
        """Document assembly"""
        # Get the current working directory
        current_dir = os.getcwd()

        # Construct the full path to the PLUTO file relative to the current working directory
        pluto_file_path = os.path.join(current_dir, self.pluto_filepath)

        # Create flowchart
        self.chart = PlutoFlowchart.create_flowchart(pluto_file_path, self.output_path)

        color_text.green(
            f"Flowchart complete, identified {len(self.chart.steps_store.steps)} steps."
        )

        # Create cover page
        cover_page = create_cover_page(pluto_file_path)

        # Create a PDF document
        color_text.cyan("Creating document...")

        doc_path = f"{self.output_path}.pdf"
        doc = SimpleDocTemplate(doc_path, **DOC_STYLE)

        # convert SVG to drawing
        drawing = svg2rlg(f"{self.output_path}.svg")
        drawing.hAlign = "CENTER"

        # scale to fit the width of an A4 page
        scale = min(doc.width / drawing.minWidth(), 0.5)
        drawing.width, drawing.height = (
            drawing.minWidth() * scale,
            drawing.height * scale,
        )
        drawing.scale(scale, scale)

        # template page that grows in length if needed
        chart_width = doc.width
        chart_height = max(drawing.height, doc.height)
        frame_chart = Frame(
            doc.leftMargin,
            doc.bottomMargin,
            chart_width,
            chart_height,
            0,
            0,
            0,
            0,
            id="flowchart",
        )
        frame_normal = Frame(
            doc.leftMargin, doc.bottomMargin, doc.width, doc.height, id="normal"
        )
        doc.addPageTemplates(
            [
                PageTemplate(
                    id="flowchart",
                    frames=frame_chart,
                    pagesize=(
                        frame_chart.width + doc.leftMargin + doc.rightMargin,
                        frame_chart.height + doc.topMargin + doc.bottomMargin,
                    ),
                ),
                PageTemplate(id="normal", frames=frame_normal),
            ]
        )

        elements = []
        elements.extend(cover_page)
        elements.append(
            NextPageTemplate("flowchart")
        )  # NextPageTemplate means use new page for flow chart
        elements.append(drawing)
        elements.append(NextPageTemplate("normal"))
        elements.append(PageBreak())
        elements.append(self.build_step_table())
        self.clean_up()  # Access the 'clean_up' method using 'self'
        doc.build(elements)
        color_text.success(f"\nDocument created at {doc_path}")


def pluto_to_pdf():
    """Used when running as a package"""

    color_text.magenta("LibreCube's PLUTO to PDF Generator")

    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="Path to input PLUTO file")
    parser.add_argument("output_name", nargs="?", help="Name of the output PDF file")

    args = parser.parse_args()

    if not args.output_name:
        args.output_name = args.input_file.replace(".pluto", "")

    PlutoDocument(args.input_file, args.output_name).create()
