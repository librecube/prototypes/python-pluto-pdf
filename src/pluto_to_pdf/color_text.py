""" ColorText class for colorama color text in the console"""

from colorama import Fore, Style


class ColorText:
    """Wrapper class for Colorama to simplify
    the use of color text in the console
    """

    def __init__(self):
        pass

    def warning(self, msg: str):
        """Yellow color text warning fuction"""
        print(Fore.YELLOW + Style.BRIGHT + msg + Style.RESET_ALL)

    def error(self, msg: str):
        """Red color text error fuction"""
        print(Fore.RED + Style.BRIGHT + msg + Style.RESET_ALL)

    def success(self, msg: str):
        """Green color text success fuction"""
        print(Fore.GREEN + Style.BRIGHT + msg + Style.RESET_ALL)

    def magenta(self, msg: str):
        """Magenta color text simplified as a fuction"""
        print(Fore.MAGENTA + Style.BRIGHT + msg + Style.RESET_ALL)

    def cyan(self, msg: str):
        """Colorama Cyan color text simplified for as a fuction"""
        print(Fore.CYAN + msg + Style.RESET_ALL)

    def green(self, msg: str):
        """Colorama Green color text simplified for as a fuction"""
        print(Fore.GREEN + msg + Style.RESET_ALL)
