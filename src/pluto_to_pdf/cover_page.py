""" This module generates a cover page for the PDF document. """

import os
import re
import time
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle
from reportlab.platypus import (
    Paragraph,
    Table,
    TableStyle,
    XPreformatted,
    Spacer,
)
from .color_text import ColorText

# Global variables
color_text = ColorText()
elements = []


def meta_data(data: str, file_path: str):
    """Extracts metadata from the PLUTO file and displays it on the cover page."""
    # Define regex patterns for each field
    title_pattern = r"Title:\s*(.*)"
    description_pattern = r"Description:\s*(.*)"
    authors_pattern = r"Authors:\s*(.*)"
    date_pattern = r"Date:\s*(.*)"
    version_pattern = r"Version:\s*(.*)"
    creation_date = time.ctime(os.path.getctime(file_path))
    file_name = os.path.basename(file_path)

    # Function to extract value or return default if not found
    def extract_value(pattern, data, default="--"):
        match = re.search(pattern, data)
        return match.group(1).strip() if match else default

    # Extract values with custom defaults
    title = extract_value(title_pattern, data, file_name)
    description = extract_value(description_pattern, data)
    authors = extract_value(authors_pattern, data)
    date = extract_value(date_pattern, data, creation_date)
    version = extract_value(version_pattern, data)

    # Create a Table with the meta data:
    meta_style = ParagraphStyle(
        name="Meta",
        fontSize=10,
        leading=12,
        alignment=0,
        textColor="#00000",
        fontName="Helvetica",
        spaceBefore=10,
    )

    meta_data_table = Table(
        [
            [Paragraph(f"<b>Title:</b> {title}", meta_style)],
            [Paragraph(f"<b>Description:</b> {description}", meta_style)],
            [Paragraph(f"<b>Authors:</b> {authors}", meta_style)],
            [Paragraph(f"<b>Date:</b> {date}", meta_style)],
            [Paragraph(f"<b>Version:</b> {version}", meta_style)],
            [Paragraph(f"<b>File:</b> {file_path}", meta_style)],
        ],
        colWidths=[450],
    )
    meta_data_table.setStyle(
        TableStyle(
            [
                ("BACKGROUND", (0, 0), (-1, -1), "#deeafc"),
                ("BOX", (0, 0), (-1, -1), 1, colors.grey),
                ("INNERGRID", (0, 0), (-1, -1), 0.5, "#bdc1c7"),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 5),
                ("RIGHTPADDING", (0, 0), (-1, -1), 5),
                ("TOPPADDING", (0, 0), (-1, -1), 5),
                ("BOTTOMPADDING", (0, 0), (-1, -1), 5),
            ]
        )
    )

    return meta_data_table


def create_cover_page(pluto_file_path: str):
    """Creates a cover page for the PDF document"""
    color_text.cyan("Generating cover page...")
    code = ""
    with open(pluto_file_path, encoding="utf-8") as f:
        code = f.read()  # Read the entire file as a single string

    cover_style = ParagraphStyle(
        name="Cover",
        fontSize=11,
        leading=14,
        alignment=1,
        textColor="#36454F",
        fontName="Helvetica-Bold",
        spaceBefore=12,
    )
    code_style = ParagraphStyle(
        name="Code",
        fontSize=9,
        leading=11,
        alignment=0,
        textColor="#36454F",
        fontName="Courier",
        spaceBefore=10,
    )

    elements.append(Paragraph("This is an Auto Generated PDF", cover_style))
    elements.append(Spacer(1, 12))
    elements.append(meta_data(code, pluto_file_path))
    elements.append(Spacer(1, 12))
    elements.append(Paragraph("PLUTO Code", cover_style))
    elements.append(Spacer(1, 12))

    # Split the code into smaller chunks
    first_page_max_lines = 50
    subsequent_page_max_lines = 67  # max no. of lines

    code_lines = code.split("\n")
    code_chunks = []

    # First page chunk
    if len(code_lines) <= first_page_max_lines:
        code_chunks.append(code_lines)
    else:
        code_chunks.append(code_lines[:first_page_max_lines])
        remaining_lines = code_lines[first_page_max_lines:]

        # Subsequent page chunks
        for i in range(0, len(remaining_lines), subsequent_page_max_lines):
            code_chunks.append(remaining_lines[i : i + subsequent_page_max_lines])

    for i, chunk in enumerate(code_chunks):
        chunk_text = "\n".join(chunk)
        code_block = XPreformatted(chunk_text, code_style)
        code_table = Table(
            [[code_block]],
            style=[
                ("BACKGROUND", (0, 0), (-1, -1), colors.whitesmoke),
                ("BOX", (0, 0), (-1, -1), 1, colors.lightgrey),
                ("ALIGN", (0, 0), (-1, -1), "LEFT"),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 10),
                ("RIGHTPADDING", (0, 0), (-1, -1), 5),
                ("TOPPADDING", (0, 0), (-1, -1), 5),
                ("BOTTOMPADDING", (0, 0), (-1, -1), 10),
            ],
        )
        elements.append(code_table)

    elements.append(Paragraph("-- End Of File --", code_style))

    return elements
